# Text Wrapping

This is a translation of various
[text wrap algorithms](http://xxyxyz.org/line-breaking/) into
Haskell. They'll start as direct translations, but then change to be
more and more Haskell.
